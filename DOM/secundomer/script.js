document.addEventListener('DOMContentLoaded', function() {
  let input = document.querySelector('.count-display');
  let button = document.querySelector('.btn');
  let stopwatchDisplay = document.querySelector('.timer');
  let current = 0;
  let timer;

  function startTimer() {
    stopwatchDisplay.textContent = --current;
    if (current <= 0)
      timer = clearInterval(timer);
  }

  function onClick() {
    timer = clearInterval(timer);

    current = +input.value;
    if (current) {
      stopwatchDisplay.textContent = current;
      timer = setInterval(startTimer, 1000);
    }
  }

  button.addEventListener('click', onClick);
});
