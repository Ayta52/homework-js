const h1 = document.createElement("h1");
const input = document.createElement("input");

document.body.append(h1);
document.body.append(input);

const dbChange = debounce(
  (e) => (document.querySelector("h1").innerHTML = e.target.value),
  300
);

document.querySelector("input").addEventListener("input", (e) => dbChange(e));

function debounce(func, wait, immediate) {
  let timeout;

  return function executedFunction() {
    const context = this;
    const args = arguments;

    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);

    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
}
