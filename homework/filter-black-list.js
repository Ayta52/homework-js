
function filterBlackList(array , blackList) {
  let result = [];
  for  ( let item of array) {
    if ( blackList.indexOf(item) === -1) {
      result.push(item);
    }
  }
  return result;
}

export default filterBlackList
