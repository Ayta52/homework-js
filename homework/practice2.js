let x1 = 10;
let y2 = 2;

let x2 = -3;
let y2 = 3;

let catheus1 = Math.abs (x1-x2);
let catheus2 = Math.abs (y1-y2);

console.log(Math.sqrt(
    Math.pow(catheus1, 2) + Math.pow(catheus2, 2)
));


let first = 0.1 + 0.2 + 0.033;
let second = 0.33334;
let precions = 3;

let firstNormalized = Math.round (
    first * Math.pow(10, precions)
);

let secondNormalized = Math.round (
    second * Math.pow(10, precions)
);

console.log('Исходные числа равны', first === second);
console.log('Числа равны', firstNormalized === secondNormalized);
console.log('Первое число больше', firstNormalized > secondNormalized);
console.log('Первое число меньше', firstNormalized < secondNormalized);


/*
Генератор случайных чисел мкжду n до m.
Учесть, что n необязательно меньше, чем m!
*/

let n = 100;
let m = 350;

/* Kolvo cifr kotorie sgeneriruytsa*/

let range = Math.abs(m - n);

// okruglenie chislo 0 do range //

let numberInRange = Math.round(Math.random() * range);

// levaya granica vozmojnogo chisla

let min = Math.min(n, m);

console.log(min + numberInRange);

/*
Vivodim otdelnuy i drobnuy chast chisla s tochnosty n.
*/

let precions = 3;
let number = 0x12f + .3 + .1;
console.log('Исходное число', number);
console.log('Целая часть числа', Math.floor(number));
console.log('Дробная часть', Math.round(number * 1 * Math.pow(10, precions)));
